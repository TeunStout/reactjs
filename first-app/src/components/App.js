import React from "react";
import "./App.sass";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

import Navigation from "./navigation/Navigation";
import Homepage from "./pages/homepage/Homepage";
import Footer from "./footer/Footer";
import Hookspage from "./pages/hookspage/Hookspage";

const App = () => {
    return (
        <div className="app">
            <Router>
                <Navigation/>
                <div className="standard-page-properties">
                    <Switch>
                        <Route exact path="/" component={Homepage}/>
                        <Route exact path="/hooks" component={Hookspage}/>
                    </Switch>
                </div>
                <Footer/>
            </Router>
        </div>
    );
};

export default App;
