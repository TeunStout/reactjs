import React from "react";
import "./Navigation.sass";
import {Link} from "react-router-dom";

const Navigation = () => {
    return (
        <nav className="navigation">
            <ul className="navigation-list">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/hooks">hooks</Link>
                </li>
                <li>
                    <Link to="">No path</Link>
                </li>
            </ul>
        </nav>
    );
};

export default Navigation;
