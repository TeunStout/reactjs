import React from "react";
import "./Homepage.sass";
import Introduction from "./introduction/Introduction";


const Homepage = () => {
    return (
        <>
            <div className="homepage-intro">
                <Introduction/>
            </div>
        </>
    );
};


export default Homepage;
