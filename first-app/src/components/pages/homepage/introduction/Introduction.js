import React from "react"
import './Introduction.sass'


const Introduction = () => {
    return (
        <div className="introduction">
            <div className="introduction-display">
                <div className="introduction-display-img"/>
                <h1>Dit ben ik!</h1>
                <p>Leuk dat je er bent! 
                    Mijn naam is Teun Stout en studeer software engineering aan de Hogeschool van Amsterdam.
                    Dit is mijn eerste app in ReactJs en probeer hiermee de library/framework een beetje te kennen.
                </p>
            </div>
        </div>
    );
};

export default Introduction;