import React from "react";
import "./Hookspage.sass";
import CountList from "./count-button/CountList";
import UseEffectHooks from "./change-doc-title/DocTitle";
import CustomHook from "./custome-hook/CustomHook";

const Hookspage = () => {
    return (
        <>
            <div className="hooks">
                <CountList/>
            </div>
            <div className="hooks">
                <UseEffectHooks/>
            </div>
            <div className="hooks">
                <CustomHook/>
                <p>（（（・×・；）））</p>
                <p>＾ω＾</p>
            </div>
        </>
    );
};

export default Hookspage;
