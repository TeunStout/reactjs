import React, {useEffect, useState} from "react";
import './DocTitle.sass'
import InputDocTitle from "./InputDocTitle";

const UseEffectHooks = () => {
    const [title, setTitle] = useState('Hello world');

    useEffect(() => {
        document.title = title;
        console.log(`You updated the document title! -> ${title}`);
    });

    return (
        <div className="useEffect">
            <h1>UseEffect hook</h1>
            <h2>Type the title you want this document to have:</h2>

            <InputDocTitle setDocTitle={setTitle}/>
        </div>
    );
}


export default UseEffectHooks;