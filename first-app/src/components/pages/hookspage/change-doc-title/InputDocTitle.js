import React, {useState} from "react";
import './InputDocTitle.sass'

const InputDocTitle = (props) => {
    const [inputText, setInputText] = useState('')

    return (
        <div className="form">
                <input
                    name="titleText"
                    inputMode="text"
                    onChange={(state) => setInputText(state.target.value)}
                />
                <button onClick={() => props.setDocTitle(inputText)}>Save title</button>
        </div>
    );
}

export default InputDocTitle;