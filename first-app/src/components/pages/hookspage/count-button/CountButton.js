import React from "react";
import "./CountButton.sass";

const CountButton = (props) => {
  return (
    <div className="count-button">
      <button onClick={() => props.setCount(props.count - 1)}> - </button>
      <div className="count-tracker">
        <p>{props.title}</p>
        <p>{props.count}</p>
      </div>
      <button onClick={() => props.setCount(props.count + 1)}> + </button>
    </div>
  );
};

export default CountButton;
