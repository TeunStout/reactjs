import React, {useState} from "react";
import "./CountList.sass";
import CountButton from "./CountButton";

const CountList = () => {
    const [rows, setRows] = useState(0);

    return (
        <>
            <h1 className="title-hook">UseState hook</h1>
            <CountButton count={rows} setCount={setRows} title="How many buttons"/>
            <ListOfRows rowCount={rows}/>
        </>
    )
};

const ListOfRows = ({rowCount}) => {
    if (rowCount < 0) return <p>┏༼ ◉ ╭╮ ◉༽┓</p>;
    const rows = Array.from(Array(rowCount).keys());

    return (
        <ul className="rows">
            {rows.map((row) => (
                <li key={row}>
                    <h1>Row {row}</h1>
                </li>
            ))}
        </ul>
    );
};

export default CountList;