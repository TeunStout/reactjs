import React from "react";
import './Custom-Hook.sass'

const CustomHook = () => {
    return (
        <>
            <h1 className="custom-hook-title">Custom hook</h1>
        </>
    )
};

export default CustomHook;