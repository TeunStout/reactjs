import React from 'react';
import ReactDOM from 'react-dom';
import './index.sass';

import App from './components/App';

// Strictmode enables extra checks in the application
ReactDOM.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>,
    document.getElementById('root')
);
